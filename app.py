# 1. 导入包 
from flask import Flask, request, jsonify
# 2. 实例化一个
app = Flask(__name__)
# 3. 编写一个接口处理方法
@app.route("/add/", methods=["GET","POST"])
def add():  
	# 3.1 从请求中获取参数
	# request.values  {"a": "1", "b": "2"}
    a = request.values.get("a")
    b = request.values.get("b")
	# 3.2 业务操作
    sum = int(a) + int(b)
	# 3.3 组装响应并返回
    return str(sum)
# 4. 挂载路由(指定接口的url路径), 声明接口接受的方法
# 5. 运行接口
@app.route("/api/sub/", methods=["POST"])
def sub():
	# request.json()  {"a": 1, "b": 2}
	if not request.json:
		return jsonify({"code": "100001", "msg": "请求类型错误", "data": None})

	if not "a" in request.json or not "b" in request.json:
		return jsonify({"code": "100002", "msg": "参数缺失", "data": None})
	
	a = request.json.get("a")
	b = request.json.get("b")
	result = str(int(a) - int(b))
	return jsonify({"code": "100000", "msg": "成功", "data": result})


if __name__ == '__main__':
	app.run(port=5005)
