# 1. 导入包
import requests
import json


base_url = "http://127.0.0.1:5005"

# 2. 组装请求
def test_add_normal():
	# url  字符串格式
    url = base_url + "/add/"
    
	# data {} 字典格式
    data = {"a": "1", "b": "2"}
    # 3. 发送请求,获取响应对象
    response = requests.post(url=url, data=data)
# 4. 解析响应
# 5. 断言结果
    assert response.text == '3'

def test_sub_normal():
    url = base_url + "/api/sub/"
    headers = {"Content-Type": "application/json"} # 1. 必须 有 headers
    data = {"a": "4", "b": "2"}
    data = json.dumps(data) # 2. 序列化成字符串
    response = requests.post(url=url, headers=headers, data=data)
    # 3. 响应解析
    resp_code = response.json().get("code") # {"code":"100000", "msg": "成功", "data": "2"}
    resp_msg = response.json().get("msg")
    resp_data = response.json().get("data")
    # print(resp_code, resp_msg, resp_data)
    assert response.status_code == 200
    assert resp_code == "100000"
    assert resp_msg == "成功"
    assert resp_data == "2"



if __name__ == '__main__':
    test_sub_normal()